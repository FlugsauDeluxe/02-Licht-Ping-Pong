/*
 *  @file    00Programm-LightPingPong-V5.ino
 *  @author  FlugsauDeluxe
 *  @date    02/03/2018
 *  @version 5.0
 *
 *  @brief In diesem Projekt soll eine elektronische Variante
 *         des Spiels Tischtennis realisiert werden. Zur Simulation
 *         des Spielballs bewegt sich ein Lichtpunkt auf einer 
 *         LED-Lichterkette. Der Spielschläger wird mithilfe 
 *         eines Taster nachgebildet. Bewegt sich der Spielball
 *         auf einen Spieler zu, so muss dieser seinen Taster
 *         betätigen, um die Richtung des Spielballs umzukehren.
 *         Der Spieler darf allerdings nur in einem vorgegeben
 *         Bereich, welcher Grüne-Zone genannt wird, seinen
 *         Taster betätigen. Verpasst der Spieler diesen
 *         Bereich verliert er ein Leben.
 *         
 *  @port Pin5 => Lange LED-Kette
 *        Pin6 => kurze LED-Kette (rechte Seite)
 *        Pin7 => kurze LED-Kette (linke Seite)
 *       
 *        Pin8  => Taster Player0
 *        Pin9  => Taster Player1
 *        Pin10 => Taster Player2
 *        Pin11 => Taster Player3
 *        
 *  @caution DON'T USE Pin 0 it is needed for randomSeed!!!
 *
 */

#include <FastLED.h>
#include <Bounce2.h>

#define NUM_LEDS0 61
#define NUM_LEDS1 30
#define DATA_PIN0 5 //lange LED-Kette
#define DATA_PIN1 6 //kurze LED-Kette (rechte Seite)
#define DATA_PIN2 7 //kurze LED-Kette (linke Seite)
#define COLOR_ORDER GRB
#define CHIPSET     WS2812B


//---------------------------------------------------------------
// BEGIN {Deklaration und Initialisierung der globalen Variablen}
  // Hilfvariable für debug
  String output;

  //speichere und manipuliere die LED Daten
  CRGB leds0[NUM_LEDS0];
  CRGB leds1[NUM_LEDS1];
  CRGB leds2[NUM_LEDS1];

  // Helligkeit der LEDs festlegen
  uint8_t gBrightness = 64;

  // Variable für Timer
  unsigned long zeit1;

  // Intervallzeit für Ballgeschwindigkeit
  unsigned int ballSpeed = 100;

  // Resetzeit für Spielneustart (zwei Taster gedrückt)
  unsigned int intervalReset = 2000;

  // Datenstruktur für Lichtballposition
  struct ballPosition {
    uint8_t chain;
    int8_t led;
  } lightPositionNew, lightPositionOld;

  // Richtung des Lichtball
  int lightDirection = 0;

  // Auswahl der LED Kette
  int lightChain = 0;

  // Tasterpin festlegen
  int pinArrayButton[] = {8, 9, 10, 11};

  // Die Anzahl der Element von pinArrayButton
  int pinArraySizeButton = 4;

  // Bounce Instanzen anlegen um Taster zu entprellen
  Bounce * Buttons = new Bounce[pinArraySizeButton];

  // Bereich festlegen in dem die einzelnen Taster gedrückt werden dürfen
  // CAUTION siehe drawLightBall und greenZone
  int areas[4][2] = {
    {0, 4},
    {56, 60},
    {0, 4},
    {0, 4}
    };
  
  // Zustände der Taster
  bool arrayButtonState[] = {HIGH, HIGH, HIGH, HIGH};
  bool arrayButtonOldState[] = {HIGH, HIGH, HIGH, HIGH};
  
  // Spielzustände
  // Spielzustand 0 => Spiel ist aktiv
  // Spielzustand 1 => Spiel ist vorbei 
  int state = 0;

  //  Leben der Spieler
  int lifePlayer[] = {3,3,3,3};

// END {Deklaration und Initialisierung der globalen Variablen}
//---------------------------------------------------------------


// ################################# SETUP ######################
// setup() wird einmal bei Programmstart ausgeführt
void setup() {
  
  // Serielle Ausgabe zum Debugen etc.
  Serial.begin(9600);

  // Zufallszahlengenerator initialisieren
  randomSeed(analogRead(0));
  
  // Pins von pinArrayButton auf OUTPUT setzten
  for (int i = 0; i < pinArraySizeButton; i++) {
    pinMode(pinArrayButton[i], INPUT_PULLUP);
  }

  // Initialisieren der LED Ketten
  // Index  Kette
  // 0      61 LEDs lange LED Kette
  // 1      30 LEDs kurze LED Kette (rechte Seite)
  // 2      30 LEDs kurze LED Kette (linke Seite)
  FastLED.addLeds<CHIPSET, DATA_PIN0, COLOR_ORDER>(leds0, NUM_LEDS0);
  FastLED.addLeds<CHIPSET, DATA_PIN1, COLOR_ORDER>(leds1, NUM_LEDS1);
  FastLED.addLeds<CHIPSET, DATA_PIN2, COLOR_ORDER>(leds2, NUM_LEDS1);

  // Initialisieren der Spieltaster
  for (int i = 0; i < pinArraySizeButton; i++) {
    Buttons[i].attach(pinArrayButton[i], INPUT_PULLUP);
    Buttons[i].interval(5);
  }

  // LED Kette initialisieren für Spielstart
  initBall(CRGB::Blue);

  // Zeitpunkt merken für Timer
  zeit1 = millis();
}


// ################################# LOOP ###############################
// loop() wird während des Programmablaufs endlos immer wieder aufgerufen
void loop() {
  
  // Prüfen ob Spiel aktiv ist
  if (state == 0) {
    
    // Prüfen ob Zeitintervall abgelaufen ist
    if (millis() - zeit1 > ballSpeed) {
      
      // Zeitstempel zurücksetzten für Timer
      zeit1 = millis();

      // Neue Ballposition berechnen
      lightPositionCalculation();

      // Prüfen ob Ballposition korrekt ist, falls Spielball bei
      //    Player0 hinter der Grüne Zone ist => case 0
      //    Player1 hinter der Grüne Zone ist => case 1
      //    Player2 hinter der Grüne Zone ist => case 2
      //    Player3 hinter der Grüne Zone ist => case 3
      //    falls Spielball korret => case 4
      switch (lightPositionCorrect()) {
        case 0:
          stateAndLifeTest(lifePlayer[0]);
          break;
        case 1:
          stateAndLifeTest(lifePlayer[1]);
          break;
        case 2:
          stateAndLifeTest(lifePlayer[2]);
          break;
        case 3:
          stateAndLifeTest(lifePlayer[3]);
          break;
        case 4:
          drawLightBall();
          break;
      }
    }
  }

  // Prüfen ob Taster innerhalb der jeweiligen "Grünen Zone" gedrückt wurde
  buttonPushed(Buttons[0], arrayButtonState[0], arrayButtonOldState[0],
               areas[0], 0, lifePlayer[0]);
  buttonPushed(Buttons[1],  arrayButtonState[1], arrayButtonOldState[1],
               areas[1], 0, lifePlayer[1]);
  buttonPushed(Buttons[2],  arrayButtonState[2], arrayButtonOldState[2],
               areas[2], 1, lifePlayer[2]);
  buttonPushed(Buttons[3],  arrayButtonState[3], arrayButtonOldState[3],
               areas[3], 2, lifePlayer[3]);

  // Prüfen ob Spiel vorbei ist
  if (state == 1) {
    
    Serial.println("Restart the game");
    
    // Prüfen ob Spielneustart durchgeführt werden soll
    doubleButtonPushed(Buttons);
  }
}

//############################################################################
//                          ### FUNCTIONS ###
//############################################################################

/* BEGIN                    ### initBall() ###
 * Setzt den RGB-Wert jeder LED auf den Spielstartzustand oder den
 * Spielendzustand  
 *    Spielstartzustand:
 *        -areas[][] => Grün
 *        -lifePlayer => Rot
 *        -Spielball => Blau
 *        -Rest => Schwarz
 *    Spielendzustand:
 *        -areas[][] => Grün
 *        -lifePlayer => Rot
 *        -Spielball => Rot
 *        -Rest => Schwarz 
 */
void initBall(CRGB neutral) {

  // Setze den RGB-Wert jeder LED auf schwarz und lösche damit alle vorherigen
  // RGB-Werte der LEDs
  fill_solid(leds0, NUM_LEDS0, CRGB::Black);
  FastLED[0].showLeds(gBrightness);

  fill_solid(leds1, NUM_LEDS1, CRGB::Black);
  FastLED[1].showLeds(gBrightness);

  fill_solid(leds2, NUM_LEDS1, CRGB::Black);
  FastLED[2].showLeds(gBrightness);


  // Setze den RGB-Wert der LEDs welche die Grünen Zone darstellen 
  // auf grün
  int greenZoneSize = 5;
  fill_solid(leds0, greenZoneSize, CRGB::Green);
  fill_solid(leds0+56, greenZoneSize, CRGB::Green);
  FastLED[0].showLeds(gBrightness);

  fill_solid(leds1, greenZoneSize, CRGB::Green);
  FastLED[1].showLeds(gBrightness);

  fill_solid(leds2, greenZoneSize, CRGB::Green);
  FastLED[2].showLeds(gBrightness);
  

  // Setze den RGB-Wert der LEDs welche die Anzahl der Leben darstellen
  // auf rot
  fill_solid(leds0, lifePlayer[0], CRGB::Red);
  fill_solid(leds0+(61-lifePlayer[1]), lifePlayer[1], CRGB::Red);
  FastLED[0].showLeds(gBrightness);

  fill_solid(leds1, lifePlayer[2], CRGB::Red);
  FastLED[1].showLeds(gBrightness);
  
  fill_solid(leds2, lifePlayer[3], CRGB::Red);
  FastLED[2].showLeds(gBrightness);  


  // Setze den RGB-Wert der neutralen LED auf Blau (Spielball) oder 
  // Rot (Game Over)
  lightPositionNew.chain = 0;
  lightPositionNew.led = 30;
  leds0[lightPositionNew.led] = neutral;
  FastLED[lightPositionNew.chain].showLeds(gBrightness);
}


/* BEGIN                    ### buttonpushed(,,,,,) ###
 * Prüft, ob ein Taster gedrückt wird.
 * Befindet sich der Spielball, zum Betätigungszeitpunkt des Taster,
 * innerhalb der zum Taster zugehörigen "Grünen Zone", ändert sich
 * die Richtung des Spielballs.
 * Befindet sich der Spielball, zum Betätigungszeitpunkt des Taster,
 * NICHT innerhalb der zum Taster zugehörigen "Grünen Zone", verliert der
 * Spieler ein Leben
 */
void buttonPushed( Bounce& Button, bool& buttonState, bool& buttonOldState,
                   int area[2], int chain, int& lifePlayer) {

  // Prüfen ob sich der Wert des Tasters geändert hat
  Button.update();

  // Lesen des Werts des Tasters
  buttonState = Button.read();

  // Prüfen ob am Taster eine fallende Flanke stattgefunden hat
  if(buttonOldState == HIGH && buttonState == LOW) {

    // Prüfen ob Taster innerhalb der richtigen grünen Zone betätigt wurde
    // falls ja
    if(lightPositionNew.chain == chain &&
       area[0] <= lightPositionNew.led && lightPositionNew.led <= area[1]){

      // Ändern der Ballrichtung
      lightDirection = -lightDirection;

      // Ändern der Ballgeschwindigkeit
      ballSpeed = random(8,90);
      //ballSpeed = 100;

    // falls nein wurde der Taster zum falschen Zeitpunkt
    // gedrückt => Lebensverlust
    } else {
      stateAndLifeTest(lifePlayer);
    }
  }

  // Speichere den aktuellen Zustand für die nächste Tasterprüfung
  buttonOldState = buttonState;
}


/* BEGIN                    ### doublebuttonpushed(,) ###
 * Prüft, ob zwei Taster für eine bestimmte Zeit (intervalReset) 
 * gedrückt werden um einen Spielneustart auszuführen.
 */
void doubleButtonPushed(Bounce * Buttons) {

  // Prüfen ob sich die Werte der Taster geändert haben
  for(int i = 0; i < pinArraySizeButton; i++) {
    Buttons[i].update();
  }

  // Prüfen ob zwei verschieden Taster gedrückt sind, falls
  //    ja   => zeit1 nicht zurücksetzten
  //    nein => zeit1 zurücksetzten
  if( !(
      (Buttons[0].read() == LOW && Buttons[1].read() == LOW) ||
      (Buttons[0].read() == LOW && Buttons[2].read() == LOW) ||
      (Buttons[0].read() == LOW && Buttons[3].read() == LOW) ||
      (Buttons[1].read() == LOW && Buttons[2].read() == LOW) ||
      (Buttons[1].read() == LOW && Buttons[3].read() == LOW) ||
      (Buttons[2].read() == LOW && Buttons[3].read() == LOW)
      ) ) {
    zeit1 = millis();
  }

  // Prüfen ob Zeitintervall (intervalReset) 
  if(millis() -  zeit1 > intervalReset) {
    Serial.println("Game will be restarted");

    // Spielerleben zurücksetzen
    for (int i = 0; i < pinArraySizeButton; i++) {
      lifePlayer[i] = 3;
    }
    // Initialisieren des Spielstartzustand
    initBall(CRGB::Blue);
    
    // Verzögerung damit sich Spieler konzentrieren können
    delay(3000);

    // Zustand auf Spiel aktiv setzten
    state = 0;
  }
}


/* BEGIN                    ### lightPositionCalculation() ###
 * Berechnen der nächste Position des Spielballs
 */
void lightPositionCalculation() {
  
  // Speicher der aktuellen Spielballposition damit die LED ausgeschalten
  // werden kann
  lightPositionOld = lightPositionNew;
  
  // Prüfen auf welcher LED-Kette sich der Spielball befindet
  //    Lange Kette => case 0
  //    Kurze Kette (rechte Seite) => case 1
  //    Kurze Kette (linke Seite)  => case 2
  switch(lightPositionOld.chain) {

    // Lange Kette
    case 0:

      // Prüfen welche LED eingeschalten werden soll
      //    Neutrale LED => zufällige Wahl des Spielzweigs
      //    andernfalls => erhöhe/erniedrige Spielballposition
      switch(lightPositionOld.led) {

        // Neutrale LED
        case 30:
        
          //Zufällige Wahl des Spielzweigs
          switch(random(0,4)) {
          //switch(3) {
            case 0:
              lightPositionNew.chain = 0;
              lightPositionNew.led = 29;
              lightDirection = -1;
              break;
            case 1:
              lightPositionNew.chain = 0;
              lightPositionNew.led = 31;
              lightDirection = 1;
              break;
            case 2:
              lightPositionNew.chain = 1;
              lightPositionNew.led = 29;
              lightDirection = -1;
              break;
            case 3:
              lightPositionNew.chain = 2;
              lightPositionNew.led = 29;
              lightDirection = -1;
              break; 
          }
          break;
              
        default:
          lightPositionNew.led = lightPositionNew.led + lightDirection; 
      }
      break;

    // Kurze Kette (rechte Seite)
    case 1:
      switch(lightPositionOld.led + lightDirection) {
        case 30:
          lightPositionNew.chain = 0;
          lightPositionNew.led = 30;
          break;
          
        default:
          lightPositionNew.led = lightPositionNew.led + lightDirection;
      }
      break;

    // Kurze Kette (linke Seite)
    case 2:
      switch(lightPositionOld.led + lightDirection) {
        case 30:
          lightPositionNew.chain = 0;
          lightPositionNew.led = 30;
          break;
          
        default:
          lightPositionNew.led = lightPositionNew.led + lightDirection;
      }
  }
}


/* BEGIN                    ### LightPositionCorrect() ###
 * Prüft, ob die neue Ballposition innerhalb des Spielfeldes liegt.
 * 
 * Spielfeldmaße:
 *  Name der Kette    Nummer der Kette    gültiger LED-Wert
 *  Lange Kette       0                   0 <= Ballposition <= 60    
 *  kurze Kette       1                   0 <= Ballposition
 *  (rechte Seite)
 *  kurze Kette       2                   0 <= Ballposition
 *  (linke Seite) 
 *  
 * Falls Ballposition 
 *  bei Player0 Spielfeld verlassen hat => return 0    
 *  bei Player1 Spielfeld verlassen hat => return 1   
 *  bei Player2 Spielfeld verlassen hat => return 2   
 *  bei Player3 Spielfeld verlassen hat => return 3   
 *  korrekt => return 4
 * 
 */
int lightPositionCorrect() {

  // Prüfen auf welcher LED-Kette sich der Spielball befindet
  //    Lange Kette => case 0
  //    Kurze Kette (rechte Seite) => case 1
  //    Kurze Kette (linke Seite)  => case 2
  switch(lightPositionNew.chain) {

    // Lange Kette
    case 0:
      if(lightPositionNew.led < 0) {
        return 0;   
      } else if (lightPositionNew.led >= 61) {
        return 1;
      } else {
        return 4;
      }
      break;

    // Kurze Kette (rechte Seite) [Player2]
    case 1:
      if(lightPositionNew.led < 0) {
        return 2;   
      } else {
        return 4;
      }
      break;
      
    // Kurze Kette (linke Seite) [Player3]
    case 2:
      if(lightPositionNew.led < 0) {
        return 3;
      } else {
        return 4;
      }
      break;  
  }
}


/* BEGIN                    ### drawLightBall() ###
 * Setzt die RGB-Werte der Leds
 */
void drawLightBall() {

  // Prüfen auf welcher LED-Kette sich der Spielball BEFUNDEN HAT und 
  // LED-Wert zurücksetzen
  //    Lange Kette => case 0
  //    Kurze Kette (rechte Seite) => case 1
  //    Kurze Kette (linke Seite)  => case 2
  switch(lightPositionOld.chain) {
    
    // Lange LED-Kette [Player0, Player1]
    case 0:
      
      // Prüfen ob Spielball außerhalb der grünen Zonen ist -> LED schwarz
      // Player0 erste Zeile der Bedingung
      // Player1 zweite Zeile der Bedingung
      if(lightPositionOld.led > areas[0][1] &&
         lightPositionOld.led < areas[1][0]) {
        leds0[lightPositionOld.led] = CRGB::Black;
        FastLED[lightPositionOld.chain].showLeds(gBrightness);
        
      // Prüfen ob Ball innerhalb der roten Zone ist -> LED rot
      // Player0 erste Zeile der Bedingung
      // Player1 zweite Zeile der Bedingung 
      } else if (lightPositionOld.led < lifePlayer[0] ||        
                 lightPositionOld.led > (areas[1][1]-lifePlayer[1])) {
        leds0[lightPositionOld.led] = CRGB::Red;
        FastLED[lightPositionOld.chain].showLeds(gBrightness);

      // Andernfalls -> LED grün
      } else {
        leds0[lightPositionOld.led] = CRGB::Green;
        FastLED[lightPositionOld.chain].showLeds(gBrightness);         
      }
      break;

    // Kurze LED-Kette (rechte Seite) [Player2]
    case 1:

      // Prüfen ob Spielball außerhalb der grünen Zonen ist -> LED schwarz
      if(lightPositionOld.led > areas[2][1]) {
        leds1[lightPositionOld.led] = CRGB::Black;
        FastLED[lightPositionOld.chain].showLeds(gBrightness);
        
      // Prüfen ob Spielball innerhalb der roten Zone ist -> LED rot  
      } else if (lightPositionOld.led < lifePlayer[2]) {
        leds1[lightPositionOld.led] = CRGB::Red;
        FastLED[lightPositionOld.chain].showLeds(gBrightness);   

      // Andernfalls -> LED grün 
      } else {
        leds1[lightPositionOld.led] = CRGB::Green;
        FastLED[lightPositionOld.chain].showLeds(gBrightness);         
      }
      break;
      
    // Kurze LED-Kette (linke Seite) [Player3]
    case 2:

      // Prüfen ob Ball außerhalb der grünen Zonen ist -> LED schwarz
      if(lightPositionOld.led > areas[3][1]) {
        leds2[lightPositionOld.led] = CRGB::Black;
        FastLED[lightPositionOld.chain].showLeds(gBrightness);
        
      // Prüfen ob Ball innerhalb der roten Zone ist -> LED rot  
      } else if (lightPositionOld.led < lifePlayer[3]) {
        leds2[lightPositionOld.led] = CRGB::Red;
        FastLED[lightPositionOld.chain].showLeds(gBrightness);

      // Andernfalls -> LED grün
      } else {
        leds2[lightPositionOld.led] = CRGB::Green;
        FastLED[lightPositionOld.chain].showLeds(gBrightness);         
      }
      break;
  }

  /* CAUTION
   * Falls der nachfolgende delay fehlt, werden alle LEDs ausgeschaltet
   */
  delay(1);

  // Prüfen auf welcher LED-Kette sich der Spielball JETZT befindet und 
  // Spielball zeichnen
  //    Lange Kette => case 0
  //    Kurze Kette (rechte Seite) => case 1
  //    Kurze Kette (linke Seite)  => case 2
  switch(lightPositionNew.chain) {

    // Lange Kette
    case 0:
      leds0[lightPositionNew.led] = CRGB::Blue;
      FastLED[lightPositionNew.chain].showLeds(gBrightness);
    break;

    // Kurze Kette (rechte Seite) 
    case 1:
      leds1[lightPositionNew.led] = CRGB::Blue;
      FastLED[lightPositionNew.chain].showLeds(gBrightness);
    break;

    // Kurze Kette (linke Seite)
    case 2:
      leds2[lightPositionNew.led] = CRGB::Blue;
      FastLED[lightPositionNew.chain].showLeds(gBrightness);
    break;
  }
}

/* BEGIN                    ### stateAndLifeTest() ###
 * Prüft ob ein Spieler noch virtuelle Leben besitzt falls
 *    ja: Wird dem Spieler ein Leben abgezogen
 *    nein: Ändern des Spielzustands auf Game Over => state = 1
 */
void stateAndLifeTest(int& lifePlayer) {
  
  // Prüfen ob der Spieler noch ein Leben hat
  if(lifePlayer <= 1) {
        lifePlayer -= 1;
        initBall(CRGB::Red);
        state = 1;
  } else {
        delay(1000);
        lifePlayer -= 1;
        initBall(CRGB::Blue);
        delay(1500);
  } 
}

